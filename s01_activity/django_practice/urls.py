from django.urls import path

from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),
	# django_practice/register
	path('register', views.register, name='register'),
	# django_practice/change_password
	path('change_password', views.change_password, name='change_password'),
	# django_practice/login
	path('login', views.login_view, name='login'),
	# django_practice/logout
	path('logout', views.logout_view, name='logout')
]

