from django.urls import path

from . import views

#path() function
#path(route, view, name),
urlpatterns = [
	path('', views.index, name='index'),
	# todolist/todoitem
	path('<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
	# todolist/register
	path('register', views.register, name='register'),
	# todolist/change_passowrd
	path('change_password', views.change_password, name='change_password'),
	# todolist/login
	path('login', views.login_view, name='login'),
	# todolist/logout
	path('logout', views.logout_view, name='logout')
]